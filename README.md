# Pitch Deriva

### Equipo

Somos el grupo **Mentés** (rescate en Húngaro). El equipo está conformado por:

* Natalia: contadora de profesiòn. Analista de viabilidad financiera.
* Aura: ingeniería mecatrónica y magíster en ingeniería biomédica. Desarrolladora y emprendedora.  
* Arbey: Ingeniero mecatrónico. Desarrollador. 
* Julian: Artista y diseñador.    
* Oscar :  Emprendedor y desarrollador. 

Hemos trabajado en el proyecto **Deriva** ( concepto principalmente propuesto por el situacionismo. En francés significa tomar una caminata sin objetivo específico, usualmente en una ciudad, que sigue la llamada del momento). 

### Problema
Después de un evento natural como temblor, inundaciòn o tormenta en el cual las personas son afectadas al grado de quedar atrapados o desaparecidas, se tiene un rango de 24 horas para ubicarlas, rescatarlas y atenderlas de manera que no se genere efectos negativos en su salud. Queremos resolver el problema de cómo brindarle a los rescatistas y voluntarios un elemento que les permita acceder o explorar lugares peligrosos para detectar factores de riesgo y en caso de ubicar personas, permitirles actuar óptimamente para su rescate.  

### Solución
La solución que proponemos es un dispositivo que pueda ser adaptado a un collar de perro para que èste durante su exploraciòn pueda registrar factores determinante para las labores de rescate. Este dispositivo se conecta con la base de datos cloudant de IBM y también con unos hololents que facilitan la visualizaciòn de la informaciòn importante para los rescatistas.  El dispositivo cuenta con un arduino, una tarjeta raspberry zero-w, una càmara de raspberry. GPS, sensor de gases MQ5. 
La tarjeta arduino recoge los datos y los envía a la raspberry zero-W, la cual por medio de comunicaciòn punto a punto los envìa a la nube de IBM cloudant y tambièn a los hololents.

[DEMO]

### Diferencial viabilidad financiera y escalabilidad
La razón por la que se eligió este tipo de dispositivo es porque los perros son màs agiles,  veloces  e intuitivos para la exploración de zonas. Además si logran acceder a un lugar de difícil acceso para los rescatistas puede causar empatìa y tranquilidad para la persona que se encuentre atrapada o en condiciòn de vulnerabilidad mientras se generan las condiciones para el acceso de los rescatistas. 
Otro factor diferenciador es la experiencia de usuario ya que se emplea realidad mixta lo cual permite a la persona hacer seguimiento y visualización de los que el perro observa y también de los elementos que él tiene a su alrededor. 

Costos: el costo de este dispositivo fue de -----.  Si se logran reproducir a gran escala podrìa costar------- .  

### Trabajo futuro:
A futuro se planea incluir comunicación con micrófono que permita integrarse con la API de speech to text y con la de análisis de personalidad para determinar mejor el estado psicológico de las personas afectadas. 
También se esperarìa incluir en la visualización del usuario otros datos de sensores biométricos (frecuencia cardiaca, tensión)  
