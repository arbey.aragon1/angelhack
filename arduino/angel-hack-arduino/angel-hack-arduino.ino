#include <SoftwareSerial.h>
#include <TinyGPS.h>

TinyGPS gps;
SoftwareSerial serialgps(4,3);

unsigned long chars;
unsigned short sentences, failed_checksum;

char dato=' ';
int sensorPin0 = A0;
int sensorPin1 = A1;
int sensorPin2 = A2;
int ledPin = 13;
int sensorValue0 = 0;
int sensorValue1 = 0;
int sensorValue2 = 0;

void setup() {
  Serial.begin(9600);
  serialgps.begin(9600);
}

void loop() {
  sensorValue0 = analogRead(sensorPin0);
  sensorValue1 = analogRead(sensorPin1);
  sensorValue2 = analogRead(sensorPin2);
  Serial.print("S0:");
  Serial.println(sensorValue0);
  Serial.print("S1:");
  Serial.println(sensorValue1);
  Serial.print("S2:");
  Serial.println(sensorValue2);
  if(serialgps.available()){
    int c = serialgps.read(); 
    if(gps.encode(c)) 
    {
      float latitude, longitude;
      gps.f_get_position(&latitude, &longitude);
      Serial.print("lat:");
      Serial.print(latitude,2);
      Serial.print(",lon:");
      Serial.println(longitude,2);         
      
      gps.stats(&chars, &sentences, &failed_checksum);
    }  
  }
  delay(1000);
}
