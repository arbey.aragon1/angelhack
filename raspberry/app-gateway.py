import serial, time
import os
from os.path import join, dirname
from dotenv import load_dotenv
import datetime

from cloudant.client import Cloudant
from cloudant.error import CloudantException
from cloudant.result import Result, ResultByKey

dotenv_path = join(dirname(__file__), '.env')
load_dotenv(dotenv_path)

print(os.getenv('URL'), os.getenv('USER_KEY'), os.getenv('PASS'))
url=os.getenv('URL')
client = Cloudant(os.getenv('USER_KEY'),os.getenv('PASS'),url=url)
client.connect()

databaseName = 'database-angel-hack'

databaseObj = client.create_database(databaseName)

if databaseObj.exists():
    print('DB correctamente creada')
    
arduino = serial.Serial(os.getenv('DEVICE_PORT'), 9600)

jsonDocument = {}
while True:
    rawString = arduino.readline()
    lista = [i.replace("\\r\\n'",'').replace("b'",'').split(':') for i in str(rawString).split(',')]
    jsonDocument = {**jsonDocument, **dict(lista), 'time': str(datetime.datetime.now())}
    
    print(jsonDocument)
    newDocument = databaseObj.create_document(jsonDocument)
    if newDocument.exists():
        print('Se creo correctamente el doc')
    time.sleep(3)
arduino.close()
print('End')