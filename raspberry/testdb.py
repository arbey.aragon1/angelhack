import os
from os.path import join, dirname
from dotenv import load_dotenv
 
from cloudant.client import Cloudant
from cloudant.error import CloudantException
from cloudant.result import Result, ResultByKey


dotenv_path = join(dirname(__file__), '.env')
load_dotenv(dotenv_path)

print(os.getenv('URL'), os.getenv('USER_KEY'), os.getenv('PASS'))
url=os.getenv('URL')
client = Cloudant(os.getenv('USER_KEY'),os.getenv('PASS'),url=url)
client.connect()

databaseName = 'database-angel-hack'

databaseObj = client.create_database(databaseName)

if databaseObj.exists():
    print('DB correctamente creada')

#sampleData = [
#   [1, "one", "boiling", 100],
#   [2, "two", "hot", 40],
#   [3, "three", "warm", 20],
#   [4, "four", "cold", 10],
#   [5, "five", "freezing", 0]
# ]
#
#for document in sampleData:
#    number = document[0]
#    name = document[1]
#    description = document[2]
#    temperature = document[3]
#
#    jsonDocument = {
#        "numberField": number,
#        "nameField": name,
#        "descriptionField": description,
#        "temperatureField": temperature
#    }
#
#    newDocument = databaseObj.create_document(jsonDocument)
#
#    if newDocument.exists():
#        print('Se creo correctamente el doc')


result_collection =  Result(databaseObj.all_docs, include_docs=True)
print(result_collection[0])

end_point = '{0}/{1}'.format(url, databaseName + "/_all_docs")
params = {'include_docs': 'true'}
response = client.r_session.get(end_point, params=params)

listaHTML=''
for i in response.json()['rows']:
    try:
        s0=i['doc']['S0']
        lon=i['doc']['lon']
        lat=i['doc']['lat']
        listaHTML+="<li><p>Latitud: {}</p><p>longitud: {}</p><p>S0: {}</p></li>".format(lat, lon, s0)
    except:
        print('No se puede leer')
        print(i)
listaHTML='<ul>'+listaHTML+'</ul>'
print(listaHTML)
#try :
#    client.delete_database(databaseName)
#except CloudantException:
#    print("There was a problem deleting '{0}'.\n".format(databaseName))
#else:
#    print ("'{0}' successfully deleted.\n".format(databaseName))
#
client.disconnect()
