import os
from os.path import join, dirname
from dotenv import load_dotenv
 
from cloudant.client import Cloudant
from cloudant.error import CloudantException
from cloudant.result import Result, ResultByKey

from flask import Flask, flash, redirect, render_template, request, session, abort

dotenv_path = join(dirname(__file__), '.env')
load_dotenv(dotenv_path)

print(os.getenv('URL'), os.getenv('USER_KEY'), os.getenv('PASS'))
url=os.getenv('URL')
client = Cloudant(os.getenv('USER_KEY'),os.getenv('PASS'),url=url)
client.connect()

databaseName = 'database-angel-hack'

databaseObj = client.create_database(databaseName)

if databaseObj.exists():
    print('DB correctamente creada')

def getListOfDots():
    result_collection =  Result(databaseObj.all_docs, include_docs=True)
    print(result_collection[0])

    end_point = '{0}/{1}'.format(url, databaseName + "/_all_docs")
    params = {'include_docs': 'true'}
    response = client.r_session.get(end_point, params=params)

    listaHTML=''
    data=[]
    for i in response.json()['rows']:
        try:
            s0=i['doc']['S0']
            lon=i['doc']['lon']
            lat=i['doc']['lat']
            data.append(
                {
                    'S0':s0,
                    'lon':lon,
                    'lat':lat
                }
            )
            listaHTML+="<li><p>Latitud: {}</p><p>longitud: {}</p><p>S0: {}</p></li>".format(lat, lon, s0)
        except:
            print('No se puede leer')
            print(i)
    listaHTML='<ul>'+listaHTML+'</ul>'
    print(listaHTML)
    return data


app = Flask(__name__)

@app.route("/")
@app.route("/home/")
def home():
    return render_template('index.html')

@app.route("/mapa/")
def map():
    return render_template('mapa.html',lista=getListOfDots())

@app.route("/video/")
def video():
    return render_template('video.html')

if __name__ == "__main__":
    app.debug = True
    app.run()

    